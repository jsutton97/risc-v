library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity alu is
    port (data_a, data_b : in std_logic_vector (31 downto 0);
          add_out, slt_out, sltu_out, xor_out, or_out, and_out, sub_out : out std_logic_vector (31 downto 0));
end;

architecture behaviour of alu is
    signal add_long, unsigned_a, unsigned_b, sub_long : unsigned (32 downto 0);
    
begin
    xor_out <= data_a xor data_b;
    and_out <= data_a and data_b;
    or_out <= data_a or data_b;

    unsigned_a <= '0' & unsigned(data_a);
    unsigned_b <= '0' & unsigned(data_b);

    add_long <= unsigned_a + unsigned_b;
    add_out <= std_logic_vector(add_long(31 downto 0));

    slt_out (31 downto 1) <= (others => '0');
    sltu_out (31 downto 1) <= (others => '0');

    slt_out (0) <= '1' when signed(data_a) < signed(data_b) else '0';
    sltu_out (0) <= '1' when unsigned(data_a) < unsigned(data_b) else '0';

    sub_long <= unsigned_a - unsigned_b;
    sub_out <= std_logic_vector(sub_long(31 downto 0));

end;