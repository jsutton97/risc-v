library ieee;
use ieee.std_logic_1164.all;

package control_signals is
    type alu_control is (
        shift_sll,
        shift_srl,
        shift_sra,
        alu_add,
        alu_slt,
        alu_sltu,
        alu_xor,
        alu_or,
        alu_and,
        alu_sub,
        md_mul,
        md_mulh,
        md_mulhu,
        md_mulhsu,
        md_div,
        md_divu,
        md_rem,
        md_remu
    );
    type immediate_select is (
        i_type,
        s_type,
        b_type,
        u_type,
        j_type
    );
    type register_write_select is (
        alu_output,
        pc_plus_4,
        memory_data,
        load_upper
    );
    type branch_select is (
        branch_eq,
        branch_neq,
        branch_lt,
        branch_ltu,
        branch_ge,
        branch_geu,
        no_branch
    );
    type memory_select is (
        word,
        half_word,
        byte
    );
end;