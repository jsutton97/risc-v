library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.control_signals.all;

entity controller is
    port (
        alu_out_control : out alu_control;
        pc_jump_control : out std_logic; -- 1 if jump instruction, 0 otherwise 
        pc_reg_pc_control : out std_logic; -- 1 if pc + imm, 0 if reg + imm
        reg_write_enable : out std_logic;
        mem_write_enable : out std_logic;
        immediate_control : out immediate_select;
        alu_a_input_select : out std_logic; -- 1 if register input, 0 if pc
        alu_b_input_select : out std_logic; -- 1 if register input, 0 if immediate
        register_control : out register_write_select;
        branch_control : out branch_select;
        memory_control : out memory_select;
        memory_sign : out std_logic; -- 1 for signed, 0 for unsigned
        control_instruction : in std_logic_vector(31 downto 0)
    );
end;


architecture behaviour of controller is
    signal opcode : std_logic_vector(6 downto 0);
    signal funct : std_logic_vector(2 downto 0);
begin

    opcode <= control_instruction(6 downto 0);
    funct <= control_instruction(14 downto 12);

    process(opcode, funct)
    begin

    -- default values
        alu_out_control <= alu_and;
        pc_jump_control <= '0';
        pc_reg_pc_control <= '1';
        reg_write_enable <= '0';
        mem_write_enable <= '0';
        immediate_control <= i_type;
        alu_a_input_select <= '1';
        alu_b_input_select <= '1';
        register_control <= alu_output;
        branch_control <= no_branch;
        memory_control <= word;
        memory_sign <= '1';

        if opcode = "0110111" then -- LUI

            immediate_control <= u_type;

            reg_write_enable <= '1';
            register_control <= load_upper;

        elsif opcode = "0010111" then -- AUIPC

            immediate_control <= u_type;

            reg_write_enable <= '1';
            register_control <= alu_output; -- write alu output into register
            alu_a_input_select <= '0'; -- program counter input
            alu_b_input_select <= '0'; --immediate input
            alu_out_control <= alu_add;

        elsif opcode = "1101111" then -- JAL

            immediate_control <= j_type;

            pc_jump_control <= '1'; -- jump
            pc_reg_pc_control <= '1'; -- add immediate to program counter

            reg_write_enable <= '1';
            register_control <= pc_plus_4; -- store original pc in register

        elsif opcode = "1100111" then -- JALR

            immediate_control <= i_type;

            pc_jump_control <= '1'; -- jump
            pc_reg_pc_control <= '0'; -- add immediate to register

            reg_write_enable <= '1';
            register_control <= pc_plus_4; -- store original pc in register

        elsif opcode = "1100011" then -- branch instruction

            immediate_control <= b_type;

            alu_a_input_select <= '1';
            alu_b_input_select <= '1'; -- register as both inputs to ALU
            alu_out_control <= alu_add;

            if funct = "000" then -- beq
                branch_control <= branch_eq;
                alu_out_control <= alu_slt;
            elsif funct = "001" then -- bne
                branch_control <= branch_neq;
                alu_out_control <= alu_slt;
            elsif funct = "100" then -- blt
                branch_control <= branch_lt;
                alu_out_control <= alu_slt;
            elsif funct = "101" then -- bge
                branch_control <= branch_ge;
                alu_out_control <= alu_slt;
            elsif funct = "110" then -- bltu
                branch_control <= branch_ltu;
                alu_out_control <= alu_sltu;
            elsif funct = "110" then -- bgeu
                branch_control <= branch_ge;
                alu_out_control <=  alu_sltu;
            end if;

        elsif opcode = "0000011" then -- load instruction

            immediate_control <= i_type;
            register_control <= memory_data;

            alu_a_input_select <= '1';
            alu_b_input_select <= '0'; -- add immediate to register
            alu_out_control <= alu_add;

            reg_write_enable <= '1';

            if funct = "000" then -- lb
                memory_control <= byte;
                memory_sign <= '1';
            elsif funct = "001" then -- lh
                memory_control <= half_word;
                memory_sign <= '1';
            elsif funct = "010" then -- lw
                memory_control <= word;
                memory_sign <= '1';
            elsif funct = "100" then -- lbu
                memory_control <= byte;
                memory_sign <= '0';
            elsif funct = "101" then -- lhu
                memory_control <= half_word;
                memory_sign <= '0';
            else
                reg_write_enable <= '0';
            end if;

        elsif opcode = "0100011" then -- store instruction

            immediate_control <= s_type;

            alu_a_input_select <= '1';
            alu_b_input_select <= '0'; -- add immediate to register
            alu_out_control <= alu_add;

            mem_write_enable <= '1';

            if funct = "000" then
                memory_control <= byte;
            elsif funct = "001" then
                memory_control <= half_word;
            elsif funct = "010" then
                memory_control <= word;
            else
                mem_write_enable <= '0';
            end if;

        elsif opcode = "0010011" then -- alu immediate

            immediate_control <= i_type;

            alu_a_input_select <= '1';
            alu_b_input_select <= '0'; -- immediate

            reg_write_enable <= '1';
            register_control <= alu_output;

            if funct = "000" then -- addi
                alu_out_control <= alu_add;
            elsif funct = "001" and control_instruction(31 downto 25) = "0000000" then -- slli
                alu_out_control <= shift_sll;
            elsif funct = "010" then -- slti
                alu_out_control <= alu_slt;
            elsif funct = "011" then -- sltiu
                alu_out_control <= alu_sltu;
            elsif funct = "100" then -- xori
                alu_out_control <= alu_xor;
            elsif funct = "101" and control_instruction(31 downto 25) = "0000000" then -- srli
                alu_out_control <= shift_srl;
            elsif funct = "101" and control_instruction(31 downto 25) = "0100000" then -- srai
                alu_out_control <= shift_sra;
            elsif funct = "110" then -- ori
                alu_out_control <= alu_or;
            elsif funct = "111" then -- andi
                alu_out_control <= alu_and;
            else
                reg_write_enable <= '0';
            end if;

        elsif opcode = "0110011" then -- alu instruction

            alu_a_input_select <= '1';
            alu_b_input_select <= '1'; -- immediate

            reg_write_enable <= '1';
            register_control <= alu_output;

            if funct = "000" and control_instruction(31 downto 25) = "0000000" then -- add
                alu_out_control <= alu_add;
            elsif funct = "000" and control_instruction(31 downto 25) = "0100000" then -- sub
                alu_out_control <= alu_sub;
            elsif funct = "001" and control_instruction(31 downto 25) = "0000000" then -- sll
                alu_out_control <= shift_sll;
            elsif funct = "010" and control_instruction(31 downto 25) = "0000000" then -- slt
                alu_out_control <= alu_slt;
            elsif funct = "011" and control_instruction(31 downto 25) = "0000000" then --sltu
                alu_out_control <= alu_sltu;
            elsif funct = "100" and control_instruction(31 downto 25) = "0000000" then -- xor
                alu_out_control <= alu_xor;
            elsif funct = "101" and control_instruction(31 downto 25) = "0000000" then -- srl
                alu_out_control <= shift_srl;
            elsif funct = "101" and control_instruction(31 downto 25) = "0100000" then -- sra
                alu_out_control <= shift_sra;
            elsif funct = "110" and control_instruction(31 downto 25) = "0000000" then -- or
                alu_out_control <= alu_or;
            elsif funct = "111" and control_instruction(31 downto 25) = "0000000" then -- and
                alu_out_control <= alu_and;
            elsif funct = "000" and control_instruction(31 downto 25) = "0000001" then -- mul
                alu_out_control <= md_mul;
            elsif funct = "001" and control_instruction(31 downto 25) = "0000001" then -- mulh
                alu_out_control <= md_mulh;
            elsif funct = "010" and control_instruction(31 downto 25) = "0000001" then -- mulhsu
                alu_out_control <= md_mulhsu;
            elsif funct = "011" and control_instruction(31 downto 25) = "0000001" then -- mulhu
                alu_out_control <= md_mulhu;
            elsif funct = "100" and control_instruction(31 downto 25) = "0000001" then -- div
                alu_out_control <= md_div;
            elsif funct = "101" and control_instruction(31 downto 25) = "0000001" then -- divu
                alu_out_control <= md_divu;
            elsif funct = "110" and control_instruction(31 downto 25) = "0000001" then -- rem
                alu_out_control <= md_rem;
            elsif funct = "111" and control_instruction(31 downto 25) = "0000001" then -- remu
                alu_out_control <= md_remu;
            else
                reg_write_enable <= '0';
            end if;

        elsif opcode = "0001111" then -- fence instruction

            if funct = "000" then
            elsif funct = "001" then
            elsif funct = "010" then
            elsif funct = "011" then
            elsif funct = "100" then
            elsif funct = "101" then
            elsif funct = "110" then
            elsif funct = "111" then
            end if;

        elsif opcode = "1110011" then -- csr

            if funct = "000" then
            elsif funct = "001" then
            elsif funct = "010" then
            elsif funct = "011" then
            elsif funct = "100" then
            elsif funct = "101" then
            elsif funct = "110" then
            elsif funct = "111" then
            end if;

        end if;


    end process;

end;