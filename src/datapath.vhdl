library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.control_signals.all;

entity datapath is
    port (
        reset, clk : in std_logic;
        alu_out_control : in alu_control;
        pc_jump_control : in std_logic; -- 1 if jump instruction, 0 otherwise
        pc_reg_pc_control : in std_logic; -- 1 if pc + imm, 0 if reg + imm
        reg_write_enable : in std_logic;
        mem_write_enable : in std_logic;
        immediate_control : in immediate_select;
        alu_a_input_select : in std_logic; -- 1 if register input, 0 if pc
        alu_b_input_select : in std_logic; -- 1 if register input, 0 if immediate
        register_control : in register_write_select;
        branch_control : in branch_select;
        memory_control : in memory_select;
        memory_sign : in std_logic; -- 1 for signed, 0 for unsigned
        control_instruction : out std_logic_vector(31 downto 0)
    );

end datapath;

architecture behaviour of datapath is

    signal instruction : std_logic_vector (31 downto 0);

    -- register file
    signal reg_data_a, reg_data_b : std_logic_vector (31 downto 0);
    signal reg_write_data : std_logic_vector (31 downto 0);

    component register_file
        port (
            r_1_addr, r_2_addr, w_addr : in unsigned(4 downto 0);
            r_1_data, r_2_data : out std_logic_vector(31 downto 0);
            clk, w_enable : in std_logic;
            w_data : in std_logic_vector(31 downto 0)
        );
    end component;

    -- main memory
    signal mem_r_addr, mem_w_addr : std_logic_vector(31 downto 0);
    signal mem_r_data, mem_w_data : std_logic_vector(31 downto 0);
    signal mem_data, mem_half_data, mem_byte_data : std_logic_vector(31 downto 0);
    signal mem_half_short : std_logic_vector(15 downto 0);
    signal mem_byte_short : std_logic_vector(7 downto 0);

    component memory
        port (
            r_1_addr, r_2_addr, w_addr : in unsigned(31 downto 0);
            r_1_data, r_2_data : out std_logic_vector(31 downto 0);
            clk, w_enable : in std_logic;
            w_data : in std_logic_vector(31 downto 0)
        );
    end component;

    -- calculations

    signal alu_a, alu_b, alu_out : std_logic_vector(31 downto 0);
    signal add_out, slt_out, sltu_out, xor_out, or_out, and_out, sub_out : std_logic_vector (31 downto 0);
    signal mul, mul_h, mul_hu, mul_hsu, div, div_u, remainder, remainder_u : std_logic_vector(31 downto 0);
    signal sll_out, srl_out, sra_out : std_logic_vector(31 downto 0);


    component alu
        port (
            data_a, data_b : in std_logic_vector (31 downto 0);
            add_out, slt_out, sltu_out, xor_out, or_out, and_out, sub_out : out std_logic_vector (31 downto 0)
        );
    end component;

    component mult_div
        port (
            data_a, data_b : in std_logic_vector(31 downto 0);
            mul, mul_h, mul_hu, mul_hsu, div, div_u, remainder, remainder_u : out std_logic_vector(31 downto 0)
        );
    end component;

    component shifter
        port (
            data : in std_logic_vector (31 downto 0);
            sll_out, srl_out, sra_out : out std_logic_vector (31 downto 0);
            shamt : in std_logic_vector (4 downto 0)
        );
    end component;

    -- immediate calculations
    signal immediate : std_logic_vector (31 downto 0);

    -- program counter
    signal pc : std_logic_vector (31 downto 0); -- current program counter
    signal pc_prime : std_logic_vector (31 downto 0); -- next value of PC
    signal pc_branch : std_logic_vector (31 downto 0); -- next value of PC from branch decision unit
    signal pc_jump : std_logic_vector (31 downto 0); -- next value of PC if jump is taken
    signal pc_imm : std_logic_vector (31 downto 0); -- next value of PC: PC or reg + sign extended immediate
    signal pc_plus_four : std_logic_vector (31 downto 0); -- typical next value of PC

    signal pc_branch_control : std_logic; -- 1 if branch is to be taken, 0 otherwise
    
    -- branch intermediate results

    signal eq, neq, lt, ltu, ge, geu : std_logic;

begin

    control_instruction <= instruction;

    registers : register_file
        port map (
            unsigned(instruction(19 downto 15)), -- rs1
            unsigned(instruction(24 downto 20)), -- rs2
            unsigned(instruction(11 downto 7)), -- rd
            reg_data_a, reg_data_b, clk, reg_write_enable, reg_write_data
        );
    
    with register_control select
        reg_write_data <= alu_out when alu_output,
                          pc_plus_four when pc_plus_4,
                          mem_data when memory_data,
                          immediate when load_upper;

    -- main memory

    main_memory : memory
        port map (
            unsigned(pc), unsigned(mem_r_addr), unsigned(mem_w_addr),
            instruction, mem_r_data, clk, mem_write_enable, mem_w_data
        );
    
    mem_w_addr <= alu_out;
    mem_r_addr <= alu_out;

    with memory_control select
        mem_data <= mem_r_data when word,
                    mem_half_data when half_word,
                    mem_byte_data when byte;
    
    mem_half_data <= X"0000" & mem_half_short when memory_sign = '0' or mem_half_short(15) = '0' else X"FFFF" & mem_half_short;
    mem_byte_data <= X"000000" & mem_byte_short when memory_sign = '0' or mem_byte_short(7) = '0' else X"FFFFFF" & mem_byte_short;

    with immediate(0) select
        mem_half_short <= mem_r_data(15 downto 0) when '0',
                          mem_r_data(31 downto 16) when '1',
                          X"0000" when others;
    
    with immediate(1 downto 0) select
        mem_byte_short <= mem_r_data(7 downto 0) when "00",
                          mem_r_data(15 downto 8) when "01",
                          mem_r_data(23 downto 16) when "10",
                          mem_r_data(31 downto 24) when "11",
                          X"00" when others;

    with memory_control select
        mem_w_data <= reg_data_b when word,
                      X"0000" & reg_data_b(15 downto 0) when half_word,
                      X"000000" & reg_data_b(7 downto 0) when byte;

    -- calculation control

    alu_a <= reg_data_a when alu_a_input_select = '1' else pc;
    alu_b <= reg_data_b when alu_b_input_select = '1' else immediate;

    alu_device: alu
        port map (alu_a, alu_b, add_out, slt_out, sltu_out, xor_out, or_out, and_out, sub_out);
    
    mult_div_device: mult_div
        port map (alu_a, alu_b, mul, mul_h, mul_hu, mul_hsu, div, div_u, remainder, remainder_u);
    
    shifter_device: shifter
        port map (alu_a, sll_out, srl_out, sra_out, alu_b (4 downto 0));
    
    with alu_out_control select
        alu_out <= sll_out     when shift_sll,
                   srl_out     when shift_srl,
                   sra_out     when shift_sra,
                   add_out     when alu_add,
                   slt_out     when alu_slt,
                   sltu_out    when alu_sltu,
                   xor_out     when alu_xor,
                   or_out      when alu_or,
                   and_out     when alu_and,
                   sub_out     when alu_sub,
                   mul         when md_mul,
                   mul_h       when md_mulh,
                   mul_hu      when md_mulhu,
                   mul_hsu     when md_mulhsu,
                   div         when md_div,
                   div_u       when md_divu,
                   remainder   when md_rem,
                   remainder_u when md_remu;


    -- immediate control

    process (immediate_control, instruction)
    begin
        immediate <= (others => instruction(31)); -- make everything sign bit
        case immediate_control is
            when i_type =>
                immediate(10 downto 0) <= instruction (30 downto 20);
            when s_type =>
                immediate(10 downto 5) <= instruction (30 downto 25);
                immediate(4 downto 0) <= instruction (11 downto 7);
            when b_type =>
                immediate(11) <= instruction(7);
                immediate(10 downto 5) <= instruction (30 downto 25);
                immediate(4 downto 1) <= instruction (11 downto 8);
                immediate(0) <= '0';
            when u_type =>
                immediate(31 downto 12) <= instruction(31 downto 12);
                immediate(11 downto 0) <= X"000";
            when j_type =>
                immediate(19 downto 12) <= instruction(19 downto 12);
                immediate(11) <= instruction(20);
                immediate(10 downto 1) <= instruction(30 downto 21);
                immediate(0) <= '0';
        end case;
    end process;
    
    -- program counter control
    process
    begin
        wait until rising_edge(clk);
        pc <= pc_prime;
    end process;

    pc_plus_four <= std_logic_vector(unsigned(pc) + 4);
    pc_imm <= std_logic_vector(signed(pc) + signed(immediate)) when pc_reg_pc_control = '1'
                else std_logic_vector(signed(reg_data_a) + signed(immediate));

    pc_prime <= X"00000000" when reset = '1' else pc_branch;
    pc_branch <= pc_imm when pc_branch_control = '1' else pc_jump;
    pc_jump <= pc_imm when pc_jump_control = '1' else pc_plus_four;


    -- branch control
    -- use slt/u on ALU
    eq <= '1' when reg_data_a = reg_data_b else '0';
    neq <= not eq;
    lt <= '1' when alu_out = X"00000001" else  '0';
    ge <= not lt;
    ltu <= '1' when alu_out = X"00000001" else  '0';
    geu <= not ltu;

    with branch_control select
        pc_branch_control <= eq when branch_eq,
                             neq when branch_neq,
                             lt when branch_lt,
                             ltu when branch_ltu,
                             ge when branch_ge,
                             geu when branch_geu,
                             '0' when no_branch;

end behaviour;