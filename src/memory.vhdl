library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity memory is
    port (r_1_addr, r_2_addr, w_addr : in unsigned(31 downto 0);
          r_1_data, r_2_data : out std_logic_vector(31 downto 0);
          clk, w_enable : in std_logic;
          w_data : in std_logic_vector(31 downto 0));
end;

architecture behaviour of memory is
    signal r_1_addr_i, r_2_addr_i, w_addr_i : natural range 0 to 4*256 - 1;
    type memory_type is array (0 to 256) of std_logic_vector(31 downto 0); 
    signal mem : memory_type;
begin
    w_addr_i <= to_integer(w_addr(31 downto 2));
    r_1_addr_i <= to_integer(r_1_addr(31 downto 2));
    r_2_addr_i <= to_integer(r_2_addr(31 downto 2));

    process
    begin
        wait until rising_edge(clk);
        if w_enable = '1' then
            mem(w_addr_i) <= w_data;
        end if;

    end process;

    r_1_data <= mem(r_1_addr_i);
    r_2_data <= mem(r_2_addr_i);

end;