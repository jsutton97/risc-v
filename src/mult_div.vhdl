library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mult_div is
    port (data_a, data_b : in std_logic_vector(31 downto 0);
          mul, mul_h, mul_hu, mul_hsu, div, div_u, remainder, remainder_u : out std_logic_vector(31 downto 0));
end;

architecture behavioural of mult_div is
    signal mul_i, mul_isu : signed(63 downto 0);
    signal mul_iu : unsigned(63 downto 0);
    signal s_a, s_b : signed(31 downto 0);
    signal u_a, u_b : unsigned(31 downto 0);

    signal long_b : signed(32 downto 0);
begin

    s_a <= signed(data_a);
    s_b <= signed(data_b);
    u_a <= unsigned(data_a);
    u_b <= unsigned(data_b);

    long_b <= '0' & signed(u_b);

    mul_i <= s_a * s_b;
    mul_iu <= u_a * u_b;
    mul_isu <= resize(s_a * long_b, 64);

    mul <= std_logic_vector(mul_i (31 downto 0));
    mul_h <= std_logic_vector(mul_i (63 downto 32));
    mul_hu <= std_logic_vector(mul_iu (63 downto 32));
    mul_hsu <= std_logic_vector(mul_isu (63 downto 32));

    div <= (others => '0');
    div_u <= (others => '0');
    remainder <= (others => '0');
    remainder_u <= (others => '0');

end;
