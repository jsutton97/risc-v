library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity register_file is
    port (r_1_addr, r_2_addr, w_addr : in unsigned(4 downto 0);
          r_1_data, r_2_data : out std_logic_vector(31 downto 0);
          clk, w_enable : in std_logic;
          w_data : in std_logic_vector(31 downto 0));
end;

architecture behaviour of register_file is
    signal r_1_addr_i, r_2_addr_i, w_addr_i : natural range 0 to 31;
    type memory_type is array (0 to 31) of std_logic_vector(31 downto 0); 
    signal memory : memory_type;
begin
    w_addr_i <= to_integer(w_addr);
    r_1_addr_i <= to_integer(r_1_addr);
    r_2_addr_i <= to_integer(r_2_addr);

    process
    begin
        wait until rising_edge(clk);
        if w_enable = '1' then
            memory(w_addr_i) <= w_data;
        end if;

    end process;

    r_1_data <= memory(r_1_addr_i) when r_1_addr_i /= 0 else (others => '0');
    r_2_data <= memory(r_2_addr_i) when r_2_addr_i /= 0 else (others => '0');

end;