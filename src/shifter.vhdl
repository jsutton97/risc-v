library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity shifter is
    port (data : in std_logic_vector (31 downto 0);
          sll_out, srl_out, sra_out : out std_logic_vector (31 downto 0);
          shamt : in std_logic_vector (4 downto 0));
end;

architecture behaviour of shifter is
    signal shamt_int : integer;
    signal zero_mask, msb_mask : std_logic_vector (31 downto 0);
begin
    shamt_int <= to_integer(unsigned(shamt));
    zero_mask <= (others => '0');
    msb_mask <= (others => data(31));

    process (shamt_int, data)
    begin
        case shamt_int is
            when 0 =>
                sll_out <= data(31 downto 0);
                srl_out <= data(31 downto 0);
                sra_out <= data(31 downto 0);
            when 1 =>
                sll_out <= data(30 downto 0) & zero_mask(0 downto 0);
                srl_out <= zero_mask(0 downto 0) & data(31 downto 1);
                sra_out <= msb_mask(0 downto 0) & data(31 downto 1);
            when 2 =>
                sll_out <= data(29 downto 0) & zero_mask(1 downto 0);
                srl_out <= zero_mask(1 downto 0) & data(31 downto 2);
                sra_out <= msb_mask(1 downto 0) & data(31 downto 2);
            when 3 =>
                sll_out <= data(28 downto 0) & zero_mask(2 downto 0);
                srl_out <= zero_mask(2 downto 0) & data(31 downto 3);
                sra_out <= msb_mask(2 downto 0) & data(31 downto 3);
            when 4 =>
                sll_out <= data(27 downto 0) & zero_mask(3 downto 0);
                srl_out <= zero_mask(3 downto 0) & data(31 downto 4);
                sra_out <= msb_mask(3 downto 0) & data(31 downto 4);
            when 5 =>
                sll_out <= data(26 downto 0) & zero_mask(4 downto 0);
                srl_out <= zero_mask(4 downto 0) & data(31 downto 5);
                sra_out <= msb_mask(4 downto 0) & data(31 downto 5);
            when 6 =>
                sll_out <= data(25 downto 0) & zero_mask(5 downto 0);
                srl_out <= zero_mask(5 downto 0) & data(31 downto 6);
                sra_out <= msb_mask(5 downto 0) & data(31 downto 6);
            when 7 =>
                sll_out <= data(24 downto 0) & zero_mask(6 downto 0);
                srl_out <= zero_mask(6 downto 0) & data(31 downto 7);
                sra_out <= msb_mask(6 downto 0) & data(31 downto 7);
            when 8 =>
                sll_out <= data(23 downto 0) & zero_mask(7 downto 0);
                srl_out <= zero_mask(7 downto 0) & data(31 downto 8);
                sra_out <= msb_mask(7 downto 0) & data(31 downto 8);
            when 9 =>
                sll_out <= data(22 downto 0) & zero_mask(8 downto 0);
                srl_out <= zero_mask(8 downto 0) & data(31 downto 9);
                sra_out <= msb_mask(8 downto 0) & data(31 downto 9);
            when 10 =>
                sll_out <= data(21 downto 0) & zero_mask(9 downto 0);
                srl_out <= zero_mask(9 downto 0) & data(31 downto 10);
                sra_out <= msb_mask(9 downto 0) & data(31 downto 10);
            when 11 =>
                sll_out <= data(20 downto 0) & zero_mask(10 downto 0);
                srl_out <= zero_mask(10 downto 0) & data(31 downto 11);
                sra_out <= msb_mask(10 downto 0) & data(31 downto 11);
            when 12 =>
                sll_out <= data(19 downto 0) & zero_mask(11 downto 0);
                srl_out <= zero_mask(11 downto 0) & data(31 downto 12);
                sra_out <= msb_mask(11 downto 0) & data(31 downto 12);
            when 13 =>
                sll_out <= data(18 downto 0) & zero_mask(12 downto 0);
                srl_out <= zero_mask(12 downto 0) & data(31 downto 13);
                sra_out <= msb_mask(12 downto 0) & data(31 downto 13);
            when 14 =>
                sll_out <= data(17 downto 0) & zero_mask(13 downto 0);
                srl_out <= zero_mask(13 downto 0) & data(31 downto 14);
                sra_out <= msb_mask(13 downto 0) & data(31 downto 14);
            when 15 =>
                sll_out <= data(16 downto 0) & zero_mask(14 downto 0);
                srl_out <= zero_mask(14 downto 0) & data(31 downto 15);
                sra_out <= msb_mask(14 downto 0) & data(31 downto 15);
            when 16 =>
                sll_out <= data(15 downto 0) & zero_mask(15 downto 0);
                srl_out <= zero_mask(15 downto 0) & data(31 downto 16);
                sra_out <= msb_mask(15 downto 0) & data(31 downto 16);
            when 17 =>
                sll_out <= data(14 downto 0) & zero_mask(16 downto 0);
                srl_out <= zero_mask(16 downto 0) & data(31 downto 17);
                sra_out <= msb_mask(16 downto 0) & data(31 downto 17);
            when 18 =>
                sll_out <= data(13 downto 0) & zero_mask(17 downto 0);
                srl_out <= zero_mask(17 downto 0) & data(31 downto 18);
                sra_out <= msb_mask(17 downto 0) & data(31 downto 18);
            when 19 =>
                sll_out <= data(12 downto 0) & zero_mask(18 downto 0);
                srl_out <= zero_mask(18 downto 0) & data(31 downto 19);
                sra_out <= msb_mask(18 downto 0) & data(31 downto 19);
            when 20 =>
                sll_out <= data(11 downto 0) & zero_mask(19 downto 0);
                srl_out <= zero_mask(19 downto 0) & data(31 downto 20);
                sra_out <= msb_mask(19 downto 0) & data(31 downto 20);
            when 21 =>
                sll_out <= data(10 downto 0) & zero_mask(20 downto 0);
                srl_out <= zero_mask(20 downto 0) & data(31 downto 21);
                sra_out <= msb_mask(20 downto 0) & data(31 downto 21);
            when 22 =>
                sll_out <= data(9 downto 0) & zero_mask(21 downto 0);
                srl_out <= zero_mask(21 downto 0) & data(31 downto 22);
                sra_out <= msb_mask(21 downto 0) & data(31 downto 22);
            when 23 =>
                sll_out <= data(8 downto 0) & zero_mask(22 downto 0);
                srl_out <= zero_mask(22 downto 0) & data(31 downto 23);
                sra_out <= msb_mask(22 downto 0) & data(31 downto 23);
            when 24 =>
                sll_out <= data(7 downto 0) & zero_mask(23 downto 0);
                srl_out <= zero_mask(23 downto 0) & data(31 downto 24);
                sra_out <= msb_mask(23 downto 0) & data(31 downto 24);
            when 25 =>
                sll_out <= data(6 downto 0) & zero_mask(24 downto 0);
                srl_out <= zero_mask(24 downto 0) & data(31 downto 25);
                sra_out <= msb_mask(24 downto 0) & data(31 downto 25);
            when 26 =>
                sll_out <= data(5 downto 0) & zero_mask(25 downto 0);
                srl_out <= zero_mask(25 downto 0) & data(31 downto 26);
                sra_out <= msb_mask(25 downto 0) & data(31 downto 26);
            when 27 =>
                sll_out <= data(4 downto 0) & zero_mask(26 downto 0);
                srl_out <= zero_mask(26 downto 0) & data(31 downto 27);
                sra_out <= msb_mask(26 downto 0) & data(31 downto 27);
            when 28 =>
                sll_out <= data(3 downto 0) & zero_mask(27 downto 0);
                srl_out <= zero_mask(27 downto 0) & data(31 downto 28);
                sra_out <= msb_mask(27 downto 0) & data(31 downto 28);
            when 29 =>
                sll_out <= data(2 downto 0) & zero_mask(28 downto 0);
                srl_out <= zero_mask(28 downto 0) & data(31 downto 29);
                sra_out <= msb_mask(28 downto 0) & data(31 downto 29);
            when 30 =>
                sll_out <= data(1 downto 0) & zero_mask(29 downto 0);
                srl_out <= zero_mask(29 downto 0) & data(31 downto 30);
                sra_out <= msb_mask(29 downto 0) & data(31 downto 30);
            when 31 =>
                sll_out <= data(0 downto 0) & zero_mask(30 downto 0);
                srl_out <= zero_mask(30 downto 0) & data(31 downto 31);
                sra_out <= msb_mask(30 downto 0) & data(31 downto 31);
            when others =>
                sll_out <= (others => '0');
                srl_out <= (others => '0');
                sra_out <= (others => '0');
        end case;
    end process;

end;