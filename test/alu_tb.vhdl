library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;


use std.textio.all;

entity alu_tb is
end;

architecture test_bench of alu_tb is
    
    type sample is record
        data_a : std_logic_vector (31 downto 0);
        data_b : std_logic_vector (31 downto 0);
        add_out : std_logic_vector (31 downto 0);
        slt_out : std_logic_vector (31 downto 0);
        sltu_out : std_logic_vector (31 downto 0);
        xor_out : std_logic_vector (31 downto 0);
        or_out : std_logic_vector (31 downto 0);
        and_out : std_logic_vector (31 downto 0);
        sub_out : std_logic_vector (31 downto 0);
    end record;

    type sample_array is array (natural range <>) of sample;
    constant test_data : sample_array :=
    (
--       data_a       data_b       add_out      slt_out      sltu_out     xor_out      or_out       and_out      sub_out
        (X"F0F0F0F0", X"F0F0F0F0", X"E1E1E1E0", X"00000000", X"00000000", X"00000000", X"F0F0F0F0", X"F0F0F0F0", X"00000000"),
        (X"00000000", X"00000000", X"00000000", X"00000000", X"00000000", X"00000000", X"00000000", X"00000000", X"00000000"),
        (X"02468ACE", X"FDB97531", X"FFFFFFFF", X"00000000", X"00000001", X"FFFFFFFF", X"FFFFFFFF", X"00000000", X"048D159D"),
        (X"02222222", X"11111111", X"13333333", X"00000001", X"00000001", X"13333333", X"13333333", X"00000000", X"F1111111"),
        (X"F0F0F0F0", X"12345678", X"03254768", X"00000001", X"00000000", X"E2C4A688", X"F2F4F6F8", X"10305070", X"DEBC9A78")
    );

    component alu
        port (data_a, data_b : in std_logic_vector (31 downto 0);
          add_out, slt_out, sltu_out, xor_out, or_out, and_out, sub_out : out std_logic_vector (31 downto 0));
    end component;
    
    signal data_a, data_b, add_out, slt_out, sltu_out, xor_out, or_out, and_out, sub_out : std_logic_vector (31 downto 0) := (others => '0');
begin
    DUT : alu
        port map (data_a, data_b, add_out, slt_out, sltu_out, xor_out, or_out, and_out, sub_out);

    process
        variable error_count : integer := 0;
        variable count : integer := 0;
    begin
        for i in test_data'range loop

            data_a <= test_data(i).data_a;
            data_b <= test_data(i).data_b;

            wait for 1 ns;

            if add_out /= test_data(i).add_out then
                error_count := error_count + 1;
                report "Test " & integer'image(count) & ": Failed due to add!" severity error;
                report "Expexcted: " & to_hstring(test_data(i).add_out) & " Actual: " & to_hstring(add_out) severity note;
            elsif slt_out /= test_data(i).slt_out then
                error_count := error_count + 1;
                report "Test " & integer'image(count) & ": Failed due to slt!" severity error;
                report "Expexcted: " & to_hstring(test_data(i).slt_out) & " Actual: " & to_hstring(slt_out) severity note;
            elsif sltu_out /= test_data(i).sltu_out then
                error_count := error_count + 1;
                report "Test " & integer'image(count) & ": Failed due to sltu!" severity error;
                report "Expexcted: " & to_hstring(test_data(i).sltu_out) & " Actual: " & to_hstring(sltu_out) severity note;
            elsif xor_out /= test_data(i).xor_out then
                error_count := error_count + 1;
                report "Test " & integer'image(count) & ": Failed due to xor!" severity error;
                report "Expexcted: " & to_hstring(test_data(i).xor_out) & " Actual: " & to_hstring(xor_out) severity note;
            elsif or_out /= test_data(i).or_out then
                error_count := error_count + 1;
                report "Test " & integer'image(count) & ": Failed due to or!" severity error;
                report "Expexcted: " & to_hstring(test_data(i).or_out) & " Actual: " & to_hstring(or_out) severity note;
            elsif and_out /= test_data(i).and_out then
                error_count := error_count + 1;
                report "Test " & integer'image(count) & ": Failed due to and!" severity error;
                report "Expexcted: " & to_hstring(test_data(i).and_out) & " Actual: " & to_hstring(and_out) severity note;
            elsif sub_out /= test_data(i).sub_out then
                error_count := error_count + 1;
                report "Test " & integer'image(count) & ": Failed due to sub!" severity error;
                report "Expexcted: " & to_hstring(test_data(i).sub_out) & " Actual: " & to_hstring(sub_out) severity note;
            else
                report "Test " & integer'image(count) & ": Passed!" severity note;
            end if;

            count := count + 1;

            wait for 1 ns;

        end loop;

        if error_count > 0 then
            report integer'image(count) & " tests run; " & integer'image(error_count) & " failure(s)" severity failure;
        else
            report integer'image(count) & " tests run; " & integer'image(error_count) & " failure(s)" severity note;
        end if;

        wait;
    end process;

end;
