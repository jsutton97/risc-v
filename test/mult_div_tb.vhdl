library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;


use std.textio.all;

entity mult_div_tb is
end;

architecture test_bench of mult_div_tb is
    
    type sample is record
        data_a : std_logic_vector (31 downto 0);
        data_b : std_logic_vector (31 downto 0);
        mul : std_logic_vector (31 downto 0);
        mul_h : std_logic_vector (31 downto 0);
        mul_hu : std_logic_vector (31 downto 0);
        mul_hsu : std_logic_vector (31 downto 0);
        div : std_logic_vector (31 downto 0);
        div_u : std_logic_vector (31 downto 0);
        remainder : std_logic_vector (31 downto 0);
        remainder_u : std_logic_vector (31 downto 0);
    end record;

    type sample_array is array (natural range <>) of sample;
    constant test_data : sample_array :=
    (
--       data_a       data_b       mul_low      mul_h        mul_hu       mul_hsu      div          div_u        rem          rem_u
        (X"00000000", X"00000001", X"00000000", X"00000000", X"00000000", X"00000000", X"00000000", X"00000000", X"00000000", X"00000000"),
        (X"00000001", X"12345678", X"12345678", X"00000000", X"00000000", X"00000000", X"00000000", X"00000000", X"00000000", X"00000000"),
        (X"10000000", X"12345678", X"80000000", X"01234567", X"01234567", X"01234567", X"00000000", X"00000000", X"00000000", X"00000000"),
        (X"80000000", X"12345678", X"00000000", X"F6E5D4C4", X"091A2B3C", X"F6E5D4C4", X"00000000", X"00000000", X"00000000", X"00000000"),
        (X"80000000", X"80000000", X"00000000", X"40000000", X"40000000", X"C0000000", X"00000000", X"00000000", X"00000000", X"00000000"),
        (X"FFFFFFFF", X"FFFFFFFF", X"00000001", X"00000000", X"FFFFFFFE", X"FFFFFFFF", X"00000000", X"00000000", X"00000000", X"00000000")
    );

    component mult_div
        port (data_a, data_b : in std_logic_vector(31 downto 0);
          mul, mul_h, mul_hu, mul_hsu, div, div_u, remainder, remainder_u : out std_logic_vector(31 downto 0));
    end component;
    
    signal data_a, data_b, mul, mul_h, mul_hu, mul_hsu, div, div_u, remainder, remainder_u : std_logic_vector (31 downto 0) := (others => '0');
begin
    DUT : mult_div
        port map (data_a, data_b, mul, mul_h, mul_hu, mul_hsu, div, div_u, remainder, remainder_u);

    process
        variable error_count : integer := 0;
        variable count : integer := 0;
    begin
        for i in test_data'range loop

            data_a <= test_data(i).data_a;
            data_b <= test_data(i).data_b;

            wait for 1 ns;

            if mul /= test_data(i).mul then
                error_count := error_count + 1;
                report "Test " & integer'image(count) & ": Failed due to mul!" severity error;
                report "Expexcted: " & to_hstring(test_data(i).mul) & " Actual: " & to_hstring(mul) severity note;
            elsif mul_h /= test_data(i).mul_H then
                error_count := error_count + 1;
                report "Test " & integer'image(count) & ": Failed due to mul_h!" severity error;
                report "Expexcted: " & to_hstring(test_data(i).mul_h) & " Actual: " & to_hstring(mul_h) severity note;
            elsif mul_hu /= test_data(i).mul_hu then
                error_count := error_count + 1;
                report "Test " & integer'image(count) & ": Failed due to mul_hu!" severity error;
                report "Expexcted: " & to_hstring(test_data(i).mul_hu) & " Actual: " & to_hstring(mul_hu) severity note;
            elsif mul_hsu /= test_data(i).mul_hsu then
                error_count := error_count + 1;
                report "Test " & integer'image(count) & ": Failed due to mul_hsu!" severity error;
                report "Expexcted: " & to_hstring(test_data(i).mul_hsu) & " Actual: " & to_hstring(mul_hsu) severity note;
            elsif div /= test_data(i).div then
                error_count := error_count + 1;
                report "Test " & integer'image(count) & ": Failed due to div!" severity error;
                report "Expexcted: " & to_hstring(test_data(i).div) & " Actual: " & to_hstring(div) severity note;
            elsif div_u /= test_data(i).div_u then
                error_count := error_count + 1;
                report "Test " & integer'image(count) & ": Failed due to div_u!" severity error;
                report "Expexcted: " & to_hstring(test_data(i).div_u) & " Actual: " & to_hstring(div_u) severity note;
            elsif remainder /= test_data(i).remainder then
                error_count := error_count + 1;
                report "Test " & integer'image(count) & ": Failed due to rem!" severity error;
                report "Expexcted: " & to_hstring(test_data(i).remainder) & " Actual: " & to_hstring(remainder) severity note;
            elsif remainder_u /= test_data(i).remainder_u then
                error_count := error_count + 1;
                report "Test " & integer'image(count) & ": Failed due to rem_u!" severity error;
                report "Expexcted: " & to_hstring(test_data(i).remainder_u) & " Actual: " & to_hstring(remainder_u) severity note;
            else
                report "Test " & integer'image(count) & ": Passed!" severity note;
            end if;

            count := count + 1;

            wait for 1 ns;

        end loop;

        if error_count > 0 then
            report integer'image(count) & " tests run; " & integer'image(error_count) & " failure(s)" severity failure;
        else
            report integer'image(count) & " tests run; " & integer'image(error_count) & " failure(s)" severity note;
        end if;

        wait;
    end process;

end;
