library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;


use std.textio.all;

entity register_file_tb is
end;

architecture test_bench of register_file_tb is
    
    type sample is record
        r_1_addr : unsigned (4 downto 0);
        r_2_addr : unsigned (4 downto 0);
        w_addr : unsigned (4 downto 0);
        r_1_data : std_logic_vector (31 downto 0);
        r_2_data : std_logic_vector (31 downto 0);
        w_data : std_logic_vector (31 downto 0);
        w_enable : std_logic;
    end record;

    type sample_array is array (natural range <>) of sample;
    constant test_data : sample_array :=
    (
--       r_1_addr r_2_addr w_addr   r_1_data     r_2_addr     w_addr       w_en
        ("00000", "00000", "00000", X"00000000", X"00000000", X"FFFFFFFF", '1'),
        ("00000", "00000", "00100", X"00000000", X"00000000", X"AAAAAAAA", '1'),
        ("00100", "00000", "01000", X"AAAAAAAA", X"00000000", X"BBBBBBBB", '1'),
        ("01000", "00100", "10000", X"BBBBBBBB", X"AAAAAAAA", X"CCCCCCCC", '1'),
        ("10000", "01000", "00100", X"CCCCCCCC", X"BBBBBBBB", X"DDDDDDDD", '0'),
        ("00100", "10000", "10101", X"AAAAAAAA", X"CCCCCCCC", X"00000000", '0')
    );

    component register_file
        port (r_1_addr, r_2_addr, w_addr : in unsigned(4 downto 0);
              r_1_data, r_2_data : out std_logic_vector(31 downto 0);
              clk, w_enable : in std_logic;
              w_data : in std_logic_vector(31 downto 0));
    end component;
    
    signal r_1_addr, r_2_addr, w_addr : unsigned(4 downto 0) := (others => '0');
    signal clk, w_enable : std_logic := '0';
    signal r_1_data, r_2_data, w_data : std_logic_vector(31 downto 0) := (others => '0');
begin
    DUT : register_file
        port map (r_1_addr, r_2_addr, w_addr, r_1_data, r_2_data, clk, w_enable, w_data);

    process
        variable count : integer := 0;
        variable error_count : integer := 0;
    begin
        for i in test_data'range loop

            r_1_addr <= test_data(i).r_1_addr;
            r_2_addr <= test_data(i).r_2_addr;
            w_addr <= test_data(i).w_addr;
            w_data <= test_data(i).w_data;
            w_enable <= test_data(i).w_enable;

            clk <= '0';

            wait for 1 ns;

            clk <= '1';

            wait for 1 ns;

            if r_1_data = test_data(i).r_1_data and r_2_data = test_data(i).r_2_data then
                report "Test " & integer'image(count) & ": Passed!" severity note;
            else
                error_count := error_count + 1;
                report "Test " & integer'image(count) & ": Failed!" severity error;
            end if;

            count := count + 1;

        end loop;

        if error_count > 0 then
            report integer'image(count) & " tests run; " & integer'image(error_count) & " failure(s)" severity failure;
        else
            report integer'image(count) & " tests run; " & integer'image(error_count) & " failure(s)" severity note;
        end if;

        wait;
    end process;

end;