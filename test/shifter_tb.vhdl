library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;


use std.textio.all;

entity shifter_tb is
end;

architecture test_bench of shifter_tb is
    
    type sample is record
        data : std_logic_vector (31 downto 0);
        sll_out : std_logic_vector (31 downto 0);
        srl_out : std_logic_vector (31 downto 0);
        sra_out : std_logic_vector (31 downto 0);
        shamt : std_logic_vector (4 downto 0);
    end record;

    type sample_array is array (natural range <>) of sample;
    constant test_data : sample_array :=
    (
--       data         sll_out      srl_out      sra_out      shamt
        (X"F0F0F0F0", X"F0F0F0F0", X"F0F0F0F0", X"F0F0F0F0", "00000"),
        (X"F0F0F0F0", X"0F0F0F00", X"0F0F0F0F", X"FF0F0F0F", "00100"),
        (X"0F0F0F0F", X"3C3C3C00", X"0003C3C3", X"0003C3C3", "01010")
    );

    component shifter
        port (data : in std_logic_vector (31 downto 0);
          sll_out, srl_out, sra_out : out std_logic_vector (31 downto 0);
          shamt : in std_logic_vector (4 downto 0));
    end component;
    
    signal data, sll_out, srl_out, sra_out : std_logic_vector (31 downto 0) := (others => '0');
    signal shamt : std_logic_vector (4 downto 0) := (others => '0');
begin
    DUT : shifter
        port map (data, sll_out, srl_out, sra_out, shamt);

    process
        variable error_count : integer := 0;
        variable count : integer := 0;
    begin
        for i in test_data'range loop

            data <= test_data(i).data;
            shamt <= test_data(i).shamt;

            wait for 1 ns;

            if sra_out = test_data(i).sra_out and srl_out = test_data(i).srl_out and sll_out = test_data(i).sll_out then
                report "Test " & integer'image(count) & ": Passed!" severity note;
            else
                error_count := error_count + 1;
                report "Test " & integer'image(count) & ": Failed! " severity error;
            end if;

            count := count + 1;

            wait for 1 ns;

        end loop;

        if error_count > 0 then
            report integer'image(count) & " tests run; " & integer'image(error_count) & " failure(s)" severity failure;
        else
            report integer'image(count) & " tests run; " & integer'image(error_count) & " failure(s)" severity note;
        end if;

        wait;
    end process;

end;
